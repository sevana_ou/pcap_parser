# This is a small C++ library to parse .pcap and .pcapng files with streaming API.

The library main advantage is that there is no need in libpcap linking. For Unix platforms libpcap usually is not a problem.
However Windows and embedded OSes can be problematic sometimes 

This library is only about parsing and does NOT generate .pcap files.
We use it in console utilities that reads .pcap files (f.e. Sevana PVQA server & PCAP analyzer).
One can read more about our tools and technologies at https://sevana.biz.

# How to use it ?

We did not provide CMakeLists project here. There are only two header & source files: pcap_parser.h and pcap_parser.cpp.
Please add them to your project and compile. Please do not forget to define TARGET_WIN or TARGET_LINUX for Windows or Linux targets.

# API.

Construct PcapDataParser or PcapNgDataParser depending on isMyFormat() class method results.
Feed binary data to parse() method.
Get callbacks within parse() call context to NetworkDumpParser::Callback instances.


# Roadmap.

Minimize header file and move most of implementations details to source file.

# License

BSD license - please see source files.


Please send us your questions & comments to development@sevana.biz

Thank you!