/* 
Copyright (c) 2020, SEVANA OÜ
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by the <organization>.
4. Neither the name of the <organization> nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY <COPYRIGHT HOLDER> ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef __PCAP_DATA_PARSER
#define __PCAP_DATA_PARSER

#include <time.h>
#include <functional>
#if defined(TARGET_WIN)
# include <WinSock2.h>
#endif

// Single captured frame
struct pcap_frame_header {
    struct timeval ts;  /* time stamp */
    uint32_t caplen; /* length of portion present */
    uint32_t len;    /* length this packet (off wire) */
};

// Abstract class for .pcap & .pcapng formats.
// Use PcapDumpParser and PcapNgDumpParser only.
class NetworkDumpParser
{
public:
    NetworkDumpParser();
    virtual ~NetworkDumpParser();

    typedef std::function<void(const pcap_frame_header* hdr, const void* data, int datalink)> Callback;
    void setCallback(Callback callback);
    Callback getCallback() const;

    virtual void parse(const void* buffer, size_t len) = 0;

    static NetworkDumpParser* make(uint32_t signature);
protected:
    // Stream buffer
    uint8_t* mBuffer = nullptr;
    size_t mBufferSize = 0, mBufferCapacity = 0;
    Callback mCallback;

    // Methods for stream buffer operations - add to the end & remove from the beginning
    void append_buffer(const void* data, size_t len);
    void erase_buffer(size_t len);
};

// Please check first 4 bytes of dump to ensure proper parser is used PcapDataParser::isMyFormat() & PcapNgDataParser::isMyFormat()
class PcapDataParser: public NetworkDumpParser
{
public:
    PcapDataParser();
    ~PcapDataParser() override;

    void parse(const void* buffer, size_t len) override;

    static bool isMyFormat(uint32_t signature);

private:
    int mState = 0;
    size_t mPacketLength = 0;
    int mDataLink = 0;
    pcap_frame_header mPacketHeader;
};

class PcapNgDataParser: public NetworkDumpParser
{
public:
    PcapNgDataParser();
    ~PcapNgDataParser() override;

    void parse(const void* buffer, size_t len) override;

    static bool isMyFormat(uint32_t signature);

private:
    enum
    {
        Block_Header,
        Block_Data,
        Block_Footer
    };
    int mBlockParseState = Block_Header;
    struct BlockHeader
    {
        uint32_t mType;
        uint32_t mSize;
    };
    BlockHeader mBlockHeader;

    size_t mPacketLength = 0;
    int mDataLink = 0;
    pcap_frame_header mPacketHeader;

    enum Endianess
    {
        Endianess_LittleEndian,
        Endianess_BigEndian
    };
    Endianess mCurrentEndianess = Endianess_LittleEndian;
    uint16_t mCurrentLinktype = 0;
    uint8_t mCurrentTimestampResolution = 6;

    void parse_block();
    void parse_shb();
    void parse_idb();
    void parse_isb();
    void parse_epb();
    void parse_spb();
    void parse_nrb();
    void parse_cb();
    void skip_block();

    uint32_t read_uint32(const void* mem);
    uint16_t read_uint16(const void* mem);
    static Endianess my_endianess();

};

#endif
