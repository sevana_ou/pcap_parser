/* 
Copyright (c) 2020, SEVANA OÜ
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by the <organization>.
4. Neither the name of the <organization> nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY <COPYRIGHT HOLDER> ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "pcap-parser.h"
#include <string>
#include <map>
#include <memory.h>

#if defined(TARGET_LINUX)
# include <arpa/inet.h>
#endif
#define INITIAL_BUFFER_CAPACITY (1024)

NetworkDumpParser::NetworkDumpParser()
{
    mBuffer = reinterpret_cast<uint8_t*>(malloc(INITIAL_BUFFER_CAPACITY));
    mBufferCapacity = INITIAL_BUFFER_CAPACITY;
    mBufferSize = 0;
}

NetworkDumpParser::~NetworkDumpParser()
{
    if (mBuffer)
        free(mBuffer);
}

NetworkDumpParser* NetworkDumpParser::make(uint32_t signature)
{
    if (PcapDataParser::isMyFormat(signature))
        return new PcapDataParser();
    if (PcapNgDataParser::isMyFormat(signature))
        return new PcapNgDataParser();
    return nullptr;
}

void NetworkDumpParser::setCallback(Callback callback)
{
    mCallback = callback;
}

NetworkDumpParser::Callback NetworkDumpParser::getCallback() const
{
    return mCallback;
}

void NetworkDumpParser::append_buffer(const void* data, size_t len)
{
    while (mBufferSize + len > mBufferCapacity)
    {
        mBuffer = reinterpret_cast<uint8_t*>(realloc(mBuffer, mBufferCapacity * 2));
        if (mBuffer)
            mBufferCapacity *= 2;
        else
            return;
    }

    memcpy(reinterpret_cast<uint8_t*>(mBuffer) + mBufferSize, data, len);
    mBufferSize += len;
}

void NetworkDumpParser::erase_buffer(size_t len)
{
    if (len >= mBufferSize)
        mBufferSize = 0;
    else
    {
        memmove(mBuffer, mBuffer + len, mBufferSize - len);
        mBufferSize -= len;
    }
}

// ------------- PcapDataParser --------------
PcapDataParser::PcapDataParser()
{
}

PcapDataParser::~PcapDataParser()
{
}

bool PcapDataParser::isMyFormat(uint32_t signature)
{
    bool normal_resolution = signature == 0xa1b2c3d4 || signature == 0xd4c3b2a1;
    bool ns_resolution = signature == 0xa1b23c4d || signature == 0x4d3cb2a1;

    return normal_resolution || ns_resolution;
}

void PcapDataParser::parse(const void* buffer, size_t len)
{
    enum
    {
        State_GlobalHeader = 0,
        State_PacketHeader,
        State_PacketData
    };

    struct pcap_file_header {
        uint32_t magic;
        uint16_t version_major;
        uint16_t version_minor;
        int32_t thiszone; /* gmt to local correction */
        uint32_t sigfigs;        /* accuracy of timestamps */
        uint32_t snaplen;        /* max length saved portion of each pkt */
        uint32_t linktype;       /* data link type (LINKTYPE_*) */
    };

    struct pcap_packet_header
    {
        uint32_t ts_1;  /* time stamp */
        uint32_t ts_2;
        uint32_t caplen;        /* length of portion present */
        uint32_t len;   /* length this packet (off wire) */
    };

    append_buffer(buffer, len);

    for (bool enough_data = true; enough_data == true;)
    {
        switch (mState)
        {
        case State_GlobalHeader:
            enough_data = mBufferSize >= sizeof(pcap_file_header);
            if (enough_data)
            {
                pcap_file_header* file_header = reinterpret_cast<pcap_file_header*>(mBuffer);

                // Check if magic ID is ok
                bool normal_resolution = file_header->magic == 0xa1b2c3d4 || file_header->magic == 0xd4c3b2a1;
                bool ns_resolution = file_header->magic == 0xa1b23c4d || file_header->magic == 0x4d3cb2a1;

                if (!normal_resolution && !ns_resolution)
                    break;

                mDataLink = static_cast<int>(file_header->linktype);
                erase_buffer(sizeof(pcap_file_header));
                mState = State_PacketHeader;
            }
            break;

        case State_PacketHeader:
            enough_data = mBufferSize >= sizeof(pcap_packet_header);
            if (enough_data)
            {
                pcap_packet_header* packet_header = reinterpret_cast<pcap_packet_header*>(mBuffer);
                mPacketHeader.ts =  {static_cast<long>(packet_header->ts_1), static_cast<long>(packet_header->ts_2)};
                mPacketHeader.len = packet_header->len;
                mPacketHeader.caplen = packet_header->caplen;
                erase_buffer(sizeof(pcap_packet_header));
                mState = State_PacketData;
            }
            break;

        case State_PacketData:
            enough_data = mBufferSize >= mPacketHeader.caplen;
            if (enough_data)
            {
                try
                {
                    mCallback(&mPacketHeader, mBuffer, mDataLink);
                }
                catch(...)
                {}
                erase_buffer(mPacketHeader.caplen);
                mState = State_PacketHeader;
            }
            break;
        }
    }
}

// ----------- PcapNgDataParser ------------
PcapNgDataParser::PcapNgDataParser()
{}

PcapNgDataParser::~PcapNgDataParser()
{}

bool PcapNgDataParser::isMyFormat(uint32_t signature)
{
    return signature == 0x0A0D0D0A;
}

void PcapNgDataParser::parse(const void* buffer, size_t len)
{
    append_buffer(buffer, len);

    bool success = false;
    do
    {
        success = false;
        if  (Block_Header == mBlockParseState)
        {
            if (mBufferSize >= 8)
            {
                mBlockHeader = *reinterpret_cast<BlockHeader*>(mBuffer);
                erase_buffer(sizeof(BlockHeader));
                mBlockParseState = Block_Data;
                success = true;
            }
        }
        else
        if (Block_Data == mBlockParseState)
        {
            if (mBufferSize >= mBlockHeader.mSize - 8)
            {
                parse_block();
                erase_buffer(mBlockHeader.mSize - 8);
                mBlockParseState = Block_Header;
                success = true;
            }
        }
    }
    while (success);
}

const uint32_t Type_SHB = 0x0A0D0D0A;
const uint32_t Type_IDB = 0x00000001;
const uint32_t Type_EPB = 0x00000006;
const uint32_t Type_SPB = 0x00000003;

void PcapNgDataParser::parse_block()
{
    switch (mBlockHeader.mType)
    {
    case Type_SHB:  parse_shb(); break;
    case Type_IDB:  parse_idb(); break;
    case Type_EPB:  parse_epb(); break;
    case Type_SPB:  parse_spb(); break;
    default:
        skip_block();
    }
}

struct option
{
    uint16_t mId;
    std::string mValue;

    int to_int() const;
};

const uint16_t opt_endofopt  = 0;
const uint16_t opt_comment = 1;
const uint16_t opt_custom_1 = 2988;
const uint16_t opt_custom_2 = 2989;
const uint16_t opt_custom_3 = 19372;
const uint16_t opt_custom_4 = 19373;

#include <map>
typedef std::map<uint16_t, option> options_map;

static uint16_t padd_32(uint16_t len)
{
    uint16_t tail = len % 4;
    return len + (tail ? 4 - tail : 0);
}

static options_map parse_options(const uint8_t* buffer, size_t maxlen)
{
    options_map r;

    struct option_header
    {
        uint16_t mId;
        uint16_t mSize;
    };

    uint16_t offset = 0;
    const option_header* hdr = nullptr;
    for (hdr = reinterpret_cast<const option_header*>(buffer + offset);
         hdr->mId != opt_endofopt && hdr->mSize && offset < maxlen;
         hdr = reinterpret_cast<const option_header*>(buffer + offset))
    {
        option opt;
        opt.mId = hdr->mId;
        opt.mValue = std::string(reinterpret_cast<const char*>(buffer + offset + sizeof(option_header)), hdr->mSize);
        r[opt.mId] = opt;

        offset += sizeof(option_header) + padd_32(hdr->mSize);
    }

    return r;
}

// available options
static const uint16_t shb_hardware = 2;
static const uint16_t shb_os = 3;
static const uint16_t shb_userappl = 4;

void PcapNgDataParser::parse_shb()
{
    struct shb_header
    {
        uint32_t mByteOrderMagic;
        uint16_t mMajorVersion;
        uint16_t mMinorVersion;
        int64_t  mSectionLength;
    };

    shb_header* hdr = reinterpret_cast<shb_header*>(mBuffer);
    if (hdr->mByteOrderMagic == 0x1A2B3C4D)
        mCurrentEndianess = Endianess_LittleEndian;
    else
        mCurrentEndianess = Endianess_BigEndian;

    auto shb_options = parse_options(mBuffer + sizeof(shb_header), mBlockHeader.mSize - sizeof(shb_header) - 12);
    // TODO: expose some SHB options
}

void PcapNgDataParser::skip_block()
{
    // Do nothing
}

void PcapNgDataParser::parse_idb()
{
    struct idb_header
    {
        uint16_t linktype;
        uint16_t reserved;
        uint32_t snaplen;
    };

    const idb_header* hdr = reinterpret_cast<const idb_header*>(mBuffer);
    mCurrentLinktype = hdr->linktype;
    auto idb_options = parse_options(mBuffer + sizeof(idb_header), mBlockHeader.mSize - sizeof(idb_header) - 12);

    // Find current timestamp resolution
    if (idb_options.count(9))
        mCurrentTimestampResolution = static_cast<uint8_t>(idb_options[9].mValue[0]);
}

void PcapNgDataParser::parse_isb()
{
    skip_block();
}

static uint64_t get_timestamp(uint64_t raw, uint8_t resolution)
{
    if (resolution & 128)
    {
        resolution &= 127;
        uint32_t two_p = static_cast<uint32_t>(0x1) << resolution;
        return static_cast<uint64_t>(static_cast<double>(raw) * (1.0 / two_p) * 100000.0);
    }
    else
    {
        if (resolution > 6)
        {
            uint32_t divider = 1;
            for (int i = 0; i < resolution - 6; i++)
                divider *= 10;
            raw /= divider;
        }
        else
        if (resolution < 6)
        {
            uint32_t multiplier = 1;
            for (int i = 0; i < 6 - resolution; i++)
                multiplier *= 10;
            raw *= multiplier;
        }
    }
    return raw;
}

void PcapNgDataParser::parse_epb()
{
    struct epb_header
    {
        uint32_t interface_id;
        uint32_t timestamp_high;
        uint32_t timestamp_low;
        uint32_t caplen;
        uint32_t origlen;
    };

    const epb_header* hdr = reinterpret_cast<const epb_header*>(mBuffer);
    uint64_t timestamp = static_cast<uint64_t>(hdr->timestamp_low) + (static_cast<uint64_t>(hdr->timestamp_high) << 32);
    timestamp = get_timestamp(timestamp, mCurrentTimestampResolution);

    if (mCallback)
    {
        pcap_frame_header frame_hdr;
        frame_hdr.ts.tv_sec = timestamp / 1000000;
        frame_hdr.ts.tv_usec = timestamp % 1000000;
        frame_hdr.len = hdr->origlen;
        frame_hdr.caplen = hdr->caplen;
        mCallback(&frame_hdr, mBuffer + sizeof(epb_header), mCurrentLinktype);
    }
}

void PcapNgDataParser::parse_spb()
{
    struct spb_header
    {
        uint32_t origlen;
    };

    const spb_header* hdr = reinterpret_cast<const spb_header*>(mBuffer);
    if (mCallback)
    {
        pcap_frame_header frame_hdr;
        frame_hdr.caplen = mBlockHeader.mSize - 12;
        if (frame_hdr.caplen > hdr->origlen)
            frame_hdr.caplen = hdr->origlen;
        frame_hdr.len = hdr->origlen;
        frame_hdr.ts.tv_sec = 0;
        frame_hdr.ts.tv_usec = 0;
        mCallback(&frame_hdr, mBuffer + sizeof(spb_header), mCurrentLinktype);
    }
}

void PcapNgDataParser::parse_nrb()
{
    skip_block();
}
void PcapNgDataParser::parse_cb()
{
    skip_block();
}

PcapNgDataParser::Endianess PcapNgDataParser::my_endianess()
{
    if (htonl(0x01020304) == 0x01020304)
        return Endianess_BigEndian;
    else
        return Endianess_LittleEndian;
}

static uint32_t change_byte_order_32(uint32_t v)
{
    uint8_t v0 = v & 0xFF,
            v1 = (v >> 8) & 0xFF,
            v2 = (v >> 16) & 0xFF,
            v3 = (v >> 24) & 0xFF;

    return v3 + (static_cast<uint32_t>(v2) << 8) + (static_cast<uint32_t>(v1) << 16) + (static_cast<uint32_t>(v0) << 24);
}

static uint16_t change_byte_order_16(uint16_t v)
{
    uint8_t v0 = v & 0xFF,
            v1 = (v >> 8) & 0xFF;

    return static_cast<uint16_t>(v1)
            + (static_cast<uint16_t>(v0) << 8);
}

uint32_t PcapNgDataParser::read_uint32(const void* mem)
{
    uint32_t raw = *reinterpret_cast<const uint32_t*>(mem);

    if (my_endianess() != mCurrentEndianess)
        return change_byte_order_32(raw);
    else
        return raw;
}

uint16_t PcapNgDataParser::read_uint16(const void* mem)
{
    uint16_t raw = *reinterpret_cast<const uint16_t*>(mem);
    if (my_endianess() != mCurrentEndianess)
        return change_byte_order_16(raw);
    else
        return raw;
}
